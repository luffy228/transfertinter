<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\ClientsRepository;
use App\Repositories\Implementation\UserRepository;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class ConnexionController extends Controller
{
    private $userRepo;
    private $clientRepo;
    use ApiResponser;


    public function __construct(UserRepository $userRepo , ClientsRepository $clientRepo)
    {
        $this->userRepo= $userRepo;
        $this->clientRepo= $clientRepo;
    }
    public function loginUser(Request $request)
    {
        $user = $this->userRepo->login($request);
        if ($user == null) {
            return $this->errorResponse("numero de telephone ou mot de passe incoreect",400);
        }else
        {
            return $this->userRepo->loginUser($user);
        }

    }

    public function logout()
    {

        $this->userRepo->logout();
        return $this->successResponse("deconnexion reussi");

    }

    public function registerUser(Request $request)
    {
        $client = $this->clientRepo->addClient($request);
        $role =   Role::find(2);
        $user = $this->userRepo->registerUser($request,$client->fresh() , $role);
        if ($user != null) {
            return $this->successResponse($user);
        }else{
            return $this->errorResponse("les informations saisies sont incorrects",400);
        }
    }
}
