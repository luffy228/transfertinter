<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\EntrepriseNumberRepository;
use App\Repositories\Implementation\OperateurRepository;
use App\Repositories\Implementation\TransactionRepository;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class TransactionController extends Controller
{

    private $entrepriseNumberRepo;
    private $operateurRepo;
    private $transactionRepo;
    use ApiResponser;



    public function __construct(EntrepriseNumberRepository $entrepriseNumberRepo , OperateurRepository $operateurRepo , TransactionRepository $transactionRepo)
    {
        $this->entrepriseNumberRepo = $entrepriseNumberRepo;
        $this->transactionRepo = $transactionRepo;
        $this->operateurRepo = $operateurRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //$user = auth()->guard('api')->user();
        //$transaction = $this->transactionRepo->adding($request , $user);
        $transaction = $this->transactionRepo->adding($request);
        // Mettre a jour les soldes dans une file
        return $this->successResponse($transaction,200);


    }

    public function findInformation(Request $request)
    {
        $data = [];
        $entreprise = $this->entrepriseNumberRepo->findNumberByOperatorExpediteur($request["operateur_expediteur"]);
        foreach ($entreprise as $admin) {
            $numero = $this->entrepriseNumberRepo->findNumberByOperator($request["operateur_destinataire"],$request["montant"],$admin["administrateur_id"]);
            if ($numero != null) {
                    array_push($data , $numero);
            }
        }


        // choisir un seul de facon aleatoire
        if (count($data) != 0) {
            $informationNumero["operateur"] = $this->entrepriseNumberRepo->getNumberByAdmin($data[0]->administrateur_id);
        $informationNumero["code"] = $this->operateurRepo->findOperateur($request["operateur_expediteur"]);
        return $this->successResponse($informationNumero);
        } else {
            $informationNumero["operateur"] = null;
            $informationNumero["code"] = $this->operateurRepo->findOperateur($request["operateur_expediteur"]);

            return $this->successResponse($informationNumero);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
