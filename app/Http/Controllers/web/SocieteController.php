<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\AdministratorRepository;
use App\Repositories\Implementation\UserRepository;
use Illuminate\Http\Request;

class SocieteController extends Controller
{
    private $userRepo;
    private $adminstrateurRepo;



    public function __construct(UserRepository $userRepo , AdministratorRepository $adminstrateurRepo)
    {
        $this->userRepo= $userRepo;
        $this->adminstrateurRepo= $adminstrateurRepo;
        $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $admin = $this->adminstrateurRepo->getAdmin();

        return view('template.backend.Listsociete', compact('admin'));

    }

    public function indexSecretaire()
    {
        //
        $admin = $this->adminstrateurRepo->getAdmin();

        return view('template.backend.Listsociete', compact('admin'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
