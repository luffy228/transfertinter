<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Administrateur extends Model
{
    use HasFactory;

    protected $fillable = [
        'nom_societe',

    ];

    public function entrepriseNumber()
    {
        return $this->hasMany('App\Models\EntrepriseNumber');
    }



    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */

    /**
     * The attributes that should be cast.
     *
     * @var array
     */

    public function usersType()
    {
        return $this->morphOne('App\Models\User','users_type');
    }

    public function nameModel()
    {
        return 'Administrateurr';
    }
}
