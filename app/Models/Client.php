<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;
    protected $fillable = [
        'pays_id',

    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */

    /**
     * The attributes that should be cast.
     *
     * @var array
     */

    public function usersType()
    {
        return $this->morphOne('App\Models\User','users_type');
    }

    public function nameModel()
    {
        return 'Administrateurr';
    }
}
