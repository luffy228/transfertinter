<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EntrepriseNumber extends Model
{
    use HasFactory;

    protected $fillable = [
        'administrateur_id',
        'operateur_id',
        'number',
        'solde'


    ];

    public function administrateur()
    {
        return $this->belongsTo('App\Models\Administrateur');
    }

    public function operateur()
    {
        return $this->belongsTo('App\Models\Operateur');
    }

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */

    /**
     * The attributes that should be cast.
     *
     * @var array
     */



    public function nameModel()
    {
        return 'EntrepriseNumber';
    }
}
