<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Secretaire extends Model
{
    use HasFactory;

    protected $fillable = [
        'administrateur_id',
];

/**
 * The attributes that should be hidden for serialization.
 *
 * @var array
 */

/**
 * The attributes that should be cast.
 *
 * @var array
 */



public function nameModel()
{
    return 'Secretaire';
}
}
