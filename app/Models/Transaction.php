<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
            'client_id',
            'expediteur_number',
            'destinataire_number',
            'entreprise_number',
            'administrateur_id',
            'commission',
            'operateur_expediteur',
            'operateur_destinataire',
            'montant',
            'statut'


    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */

    /**
     * The attributes that should be cast.
     *
     * @var array
     */



    public function nameModel()
    {
        return 'Transaction';
    }
}
