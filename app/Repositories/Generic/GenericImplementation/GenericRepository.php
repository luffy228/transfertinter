<?php

namespace  App\Repositories\Generic\GenericImplementation;

use App\Models\User;
use App\Repositories\Generic\GenericInterface\GenericRepositoryInterface;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

abstract class GenericRepository implements GenericRepositoryInterface
{
    use ApiResponser;
    private $app;
    protected $model;

    public function __construct(App $app)
    {
        $this->app = $app;
        $this->makeModel();

    }
    abstract function model();

    public function getModel()
    {
        return $this->model;
    }
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    public function all()
    {
        return $this->model->get();
    }
    public function create(array $data)
    {

        try {
            return $this->model->create($data);
        } catch (QueryException $e) {
            throw new QueryException($e->getMessage(),$data,$e->getPrevious());
        }

    }

    public function login(Request $data)
    {
        // Form Request
        if(Auth::attempt(['telephone' => $data->get('phone'), 'password' =>$data->get('password') ]))
        {
            $user = Auth::user();
            return $user;
        }
        else{
            return null;
        }
    }



    public function logout()
    {
        $token = Auth::guard('api')->user()->token();
        $token->revoke();
        return $this->successResponse(null, 'You have been successfully logged out!', 200);
    }

    public function update(array $data, $id)
    {
        $record = $this->model->find($id);
        if (!$record) {
            throw new ModelNotFoundException( app($this->model())->nameModel() .' not found');
        }
        try {
            return $record->update($data);
        } catch (QueryException $e) {
            throw new QueryException($e->getMessage(),$data,$e->getPrevious());
        }

    }
    public function find($id)
    {
        $record = $this->getModel()->where('id',$id)->first();
        return $record;
    }
    public function findLibelle($libelle)
    {
        $record = $this->getModel()->where('libelle',$libelle)->first();
        if (!$record) {
            throw new ModelNotFoundException( app($this->model())->nameModel() .' not found');
        }
        return $record;
    }
    public function delete($id)
    {
        try {
            return $this->model->delete($id);
        } catch (QueryException $e) {
            throw new QueryException($e->getMessage(),[$id],$e->getPrevious());
        }

    }

    public function show($id)
    {
        try {
            return $this->model->findOrFail($id);
        } catch (QueryException $e) {
            throw new ModelNotFoundException( app($this->model())->nameModel() .' not found');
        }

    }


    public function restore(Model $model)
    {
        // step by step cela aussi
    }

    public function updatePassword(Request $data)
    {
        // form Request
        $user = auth()->guard('api')->user();
        if (Auth::attempt(['phone' => $user->phone, 'password' => $data->get('oldPassword')])) {
            $update = User::find($user->id);
            $update->password = bcrypt($data->get("password"));
            $update->save();
            return $this->successResponse($update, 'Successfully updated password', 200);

        } else {
            return $this->errorExceptionResponse('Authentification failled: wrong password incorrect', 'WRONG_PASSWORD_EXCEPTION', 403);

        }
    }

    public function findCountry($name)
    {
        $record = $this->getModel()->where('name',$name)->first();
        return $record;
    }

    public function findName($name)
    {
        $record = $this->getModel()->where('name',$name)->first();
        return $record;
    }


   public function makeModel()
   {
       $model = app($this->model());
       if (!$model instanceof Model){}
       return $this->model = $model->newQuery();
   }
}
