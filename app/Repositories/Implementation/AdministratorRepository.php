<?php
namespace App\Repositories\Implementation;
use App\Traits\ApiResponser;
use App\Http\Requests\administratorRequest;
use App\Models\Administrateur;
use App\Models\Administrator;
use App\Models\User;
use App\Repositories\Generic\GenericImplementation\GenericRepository;
use Illuminate\Http\Request;

class AdministratorRepository extends GenericRepository
{
    use ApiResponser;

    public function model()
    {
        return 'App\Models\Administrateur';
    }

    public function register(Request $request)
    {
        $formRequest = [
            'nom_societe'=> $request["societe"]
        ];
        return $this->getModel()->create($formRequest);
    }

    public function getAdmin()
    {

        //return $admin[0]['usersType']->with('country');
        $record = $this->getModel()
                    ->whereNotIn('administrateurs.id',[1])
                    ->join('users','users.users_type_id','=','administrateurs.id')
                    ->where('users.users_type_type', "App\Models\Administrateur")
                    ->get();
        return $record;

    }



}
