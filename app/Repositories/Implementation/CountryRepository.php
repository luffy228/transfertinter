<?php
namespace App\Repositories\Implementation;
use App\Traits\ApiResponser;
use App\Http\Requests\administratorRequest;
use App\Http\Requests\CountryRequest;
use App\Models\User;
use App\Repositories\Generic\GenericImplementation\GenericRepository;

class CountryRepository extends GenericRepository
{
    use ApiResponser;

    public function model()
    {
        return 'App\Models\Country';
    }


    public function add(CountryRequest $request)
    {
        $formRequest = [
            'name' => $request["name"],
            'indicatif' => $request["indicatif"],
        ];
        $this->getModel()->create($formRequest);
        return $this->successResponse(null, "Country add successfully", 201);
    }

    public function list()
    {
        $data=$this->getModel()->get();
        return $this->successResponse($data, "Country list", 201);
    }

    public function userCountry(User $user)
    {
        $info = $user->country;
        if ($info != null) {
            return $info;
        }else {
            return $this->successResponse(null,"Store doesn't exist",200);
        }

    }



}
