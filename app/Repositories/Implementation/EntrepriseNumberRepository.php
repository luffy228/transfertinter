<?php
namespace App\Repositories\Implementation;
use App\Traits\ApiResponser;
use App\Http\Requests\administratorRequest;
use App\Models\Administrateur;
use App\Models\Administrator;
use App\Models\User;
use App\Repositories\Generic\GenericImplementation\GenericRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EntrepriseNumberRepository extends GenericRepository
{
    use ApiResponser;

    public function model()
    {
        return 'App\Models\EntrepriseNumber';
    }

    public function adding(Request $request , User $user)
    {
        $formRequest = [
            'number'=> $request["number"],
            'administrateur_id'=> $user["users_type_id"],
            'operateur_id' => $request["operateur"],
            'solde' => $request["solde"]
        ];
        return $this->getModel()->create($formRequest);
    }

    public function getNumberBySociete(User $user)
    {
        $record = DB::table('entreprise_numbers')
                        ->where('administrateur_id',$user["users_type_id"])
                        ->get();
        return $record;
    }

    public function getNumberByAdmin(string $admin_id)
    {
        $record = DB::table('entreprise_numbers')
                        ->where('administrateur_id',$admin_id)
                        ->get();
        return $record;
    }

    public function findNumberByOperatorExpediteur(string $operateur)
    {
        $record = $this->getModel()->with('operateur')
                        ->where('operateur_id',$operateur)
                        ->get();
        return $record;
    }

    public function findNumberByOperator(string $operateur , string $montant , string $administrateur_id)
    {
        $record = DB::table('entreprise_numbers')
                        ->where('administrateur_id',$administrateur_id)
                        ->where('operateur_id',$operateur)
                        ->where('solde','<' ,$montant)
                        ->first();
        return $record;
    }








}
