<?php
namespace App\Repositories\Implementation;
use App\Traits\ApiResponser;
use App\Http\Requests\administratorRequest;
use App\Jobs\TransactionJob;
use App\Models\Administrateur;
use App\Models\Administrator;
use App\Models\User;
use App\Repositories\Generic\GenericImplementation\GenericRepository;
use Illuminate\Http\Request;

class TransactionRepository extends GenericRepository
{
    use ApiResponser;

    public function model()
    {
        return 'App\Models\Transaction';
    }

    //public function adding(Request $request , User $user)
    public function adding(Request $request)
    {
        if ($request["operateur_destinataire"] == 1) {
            $entrepriseNumber = "70517794";
        } else {
            $entrepriseNumber = "99230353";
        }
        $formRequest = [
            //'client_id'=> $user["users_type_id"],
            'client_id'=> 1,
            'expediteur_number'=> $request["expediteur_number"],
            'destinataire_number'=> $request["destinataire_number"],

            'entreprise_number'=> $entrepriseNumber,
            //'entreprise_number'=> $request["entreprise_number"],
            'administrateur_id'=> 1,
            'commission'=> 10,
            'operateur_expediteur'=> $request["operateur_expediteur"],
            'operateur_destinataire'=> $request["operateur_destinataire"],


            'montant'=> $request["montant"],
            'statut'=> "Attente"
        ];
        TransactionJob::dispatch($formRequest)->delay(30);
        return $this->getModel()->create($formRequest);
    }







}
