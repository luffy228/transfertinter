<?php

namespace App\Repositories\Implementation;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Support\Str;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Repositories\Implementation\RoleRepository;
use App\Repositories\Generic\GenericImplementation\GenericRepository;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;


class UserRepository extends GenericRepository
{
    //use ApiResponser;
    private $roleRepository;

    private $countryRepository;
    private $codeOtpRepository;



    public function __construct(RoleRepository $roleRepository ) {
        $this->roleRepository=$roleRepository;



    }


    public function registerUser(Request $request , Model $model, Role $role)
    {


        if($model instanceof Model) {
            $user = new User();

            $user->nom =$request["nom"];
            $user->prenoms =$request["prenom"];
            $user->telephone = $request["telephone"];
            $user->usersType()->associate($model);
            $user->password =bcrypt($request["password"]);
            $user->save();
            $user->assignRole($role);
            return $user;
            //return $this->successResponse($user, 'user logged successfully', 200);
        }
        return null;
    }

    public function verifyNumber(string $phone)
    {
        $record = User::where('phone',$phone)->first();
        return $record;
    }

    public function loginUser(User  $user)
    {
            $data['token'] =  $user->createToken('token')->accessToken;
            $data['role']=$user->getRoleNames();
            $data['user']=$user;
            return $this->successResponse($data, 'user logged successfully', 200);

    }



    public function updateInformation(Request $request)
    {
        $form_request = [];
        if ($request["firstName"] != null) {
           $form_request['firstName'] = $request["firstName"];
        }
        if ($request["lastName"] != null) {
            $form_request['lastName'] = $request["lastName"];
         }
         if ($request["phone"] != null) {
            $form_request['phone'] = $request["phone"];
         }
         if ($request["email"] != null) {
            $form_request['email'] = $request["email"];
         }

        User::where('id',$request["user"])
                ->update($form_request);

        $user = User::find($request["user"]);
        $country = $user->country;
        $data['user']=$user;


        //return $this->successResponse($data, 'Successfully updated password', 200);

    }




    //login pour distributeur



    public function model()
    {
        return 'App\Models\User';
    }

}
