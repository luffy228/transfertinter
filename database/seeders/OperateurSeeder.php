<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OperateurSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('operateurs')->insertOrIgnore([
            ['nom' => 'Tmoney','ussd_code'=>'*145*1*montant*numero*2*pin#', 'pays_id'=>1],
            ['nom' => 'Flooz','ussd_code'=>'*155*1*numero*montant*pin#', 'pays_id'=>1],
        ]);
    }
}
