@extends('template.template')

@section('css')
    <link rel="stylesheet" href=" {{asset('assets/css/lib/datatable/dataTables.bootstrap.min.css')}} ">
@endsection


@section('contenu')
<div class="animated fadeIn">
    <!-- Widgets  -->


    <!-- /Widgets -->
    <!--  Traffic  -->

    <!--  /Traffic -->
    <div class="clearfix"></div>
    <!-- Orders -->
    <div class="orders">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="box-title" >Liste des secretaires
                            <a class="btn btn-primary"  href="#ModiferModal" data-toggle="modal" >Ajouter une secretaire</a>
                        </h4>
                        <!-- Modal -->
                        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="ModiferModal" class="modal fade">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">

                                  <h5 class="modal-title">Ajouter une secretaire</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                            <form method="post" action="{{route('storesecretaire')}}" >
                                   @csrf
                                <div class="modal-body">

                                    <div class="col-lg-12">

                                    </div>
                                        <label for="ccomment" class="control-label col-lg-5">Nom de la societe</label>
                                        <div class="col-lg-10">
                                            <select data-placeholder="Choix de la societe" class="standardSelect form-control" name="admin" tabindex="1">
                                                <option value="" label="default" disabled></option>
                                                @foreach ($societe as $societe )
                                                    <option value="{{$societe["id"]}}">{{$societe["nom_societe"]}}</option>
                                                @endforeach
                                            </select>
                                        </div>



                                        <label for="cname" class="control-label col-lg-5">Nom secretaire</label>
                                        <div class="col-lg-10">
                                          <input class=" form-control" id="cnames" name="nom" minlength="2" type="text" required />
                                        </div>



                                        <label for="ccomment" class="control-label col-lg-8">Prenoms secretaire</label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" id="cnamess" name="prenom" minlength="2" type="text" required />
                                        </div>



                                        <label for="ccomment" class="control-label col-lg-8">Telephone secretaire</label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" id="cnamess" name="telephone" minlength="2" type="text" required />
                                        </div>

                                        <label for="ccomment" class="control-label col-lg-8">Mot de passe secretaire</label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" id="cnamess" name="password" minlength="2" type="text" required />
                                        </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-6 em">
                                            <button class="btn btn-primary" type="submit">Valider</button>
                                        </div>

                                      </div>
                                </div>
                                </form>
                              </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-body--">
                        <div class="table-stats order-table ov-h">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Information Secretaire</th>
                                        <th>Societe</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($admin as $admin)
                                    <tr>

                                        <td>  <span class="name">{{$admin["nom"]}} {{$admin["prenoms"]}} / Telephone: {{$admin["telephone"]}}</span> </td>
                                        <td> <span class="product"></span>{{$admin["nom_societe"]}} </td>
                                        <td><span class="name"></span></td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> <!-- /.card -->
            </div>  <!-- /.col-lg-8 -->

             <!-- /.col-md-4 -->
        </div>
    </div>
    <!-- /.orders -->
    <!-- To Do and Live Chat -->

    <!-- /To Do and Live Chat -->
    <!-- Calender Chart Weather  -->

    <!-- /Calender Chart Weather -->
    <!-- Modal - Calendar - Add New Event -->

    <!-- /#event-modal -->
    <!-- Modal - Calendar - Add Category -->

<!-- /#add-category -->
</div>
@section('js')
<script src="{{asset('assets/js/lib/data-table/datatables.min.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/jszip.min.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/vfs_fonts.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/buttons.html5.min.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/buttons.print.min.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/buttons.colVis.min.js') }}"></script>
<script src="{{asset('assets/js/init/datatables-init.js') }}"></script>


<script type="text/javascript">
    $(document).ready(function() {
      $('#bootstrap-data-table-export').DataTable();
  } );
</script>

@endsection
@endsection
