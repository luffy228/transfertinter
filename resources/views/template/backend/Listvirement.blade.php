@extends('template.template')

@section('css')
    <link rel="stylesheet" href=" {{asset('assets/css/lib/datatable/dataTables.bootstrap.min.css')}} ">
@endsection


@section('contenu')
<div class="animated fadeIn">
    <!-- Widgets  -->

    <div class="row">
        <div class="col-lg-3 col-md-6">
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-five">
                        <div class="stat-icon dib">
                            <i class="ti-user text-primary border-primary"></i>
                        </div>
                        <div class="stat-content dib">
                            <div class="text-left dib">
                                <div class="stat-text"><span class="count">1</span></div>
                                <div class="stat-heading">Nombre de virement</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-five">
                        <div class="stat-icon dib flat-color-2">
                            <i class="pe-7s-cart"></i>
                        </div>
                        <div class="stat-content">
                            <div class="text-left dib">
                                <div class="stat-text"><span class="count">3435</span></div>
                                <div class="stat-heading">Somme E-Solux</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




    </div>
    <!-- /Widgets -->
    <!--  Traffic  -->

    <!--  /Traffic -->
    <div class="clearfix"></div>
    <!-- Orders -->
    <div class="orders">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="box-title" >Liste des virements recus
                        </h4>
                        <!-- Modal -->
                    </div>
                    <div class="card-body--">
                        <div class="table-stats order-table ov-h">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Societe</th>
                                        <th>Somme Envoye</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>  <span class="name">Louis Stanley</span> </td>
                                        <td> <span class="product">iMax</span> </td>
                                        <td><span class="count">231</span></td>
                                    </tr>

                                    <tr>
                                        <td>  <span class="name">Louis Stanley</span> </td>
                                        <td> <span class="product">iMax</span> </td>
                                        <td><span class="count">231</span></td>
                                    </tr>
                                    <tr>
                                        <td>  <span class="name">Louis Stanley</span> </td>
                                        <td> <span class="product">iMax</span> </td>
                                        <td><span class="count">231</span></td>
                                    </tr>



                                </tbody>
                            </table>
                        </div> <!-- /.table-stats -->
                    </div>
                </div> <!-- /.card -->
            </div>  <!-- /.col-lg-8 -->

             <!-- /.col-md-4 -->
        </div>
    </div>
    <!-- /.orders -->
    <!-- To Do and Live Chat -->

    <!-- /To Do and Live Chat -->
    <!-- Calender Chart Weather  -->

    <!-- /Calender Chart Weather -->
    <!-- Modal - Calendar - Add New Event -->

    <!-- /#event-modal -->
    <!-- Modal - Calendar - Add Category -->

<!-- /#add-category -->
</div>
@section('js')
<script src="{{asset('assets/js/lib/data-table/datatables.min.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/jszip.min.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/vfs_fonts.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/buttons.html5.min.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/buttons.print.min.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/buttons.colVis.min.js') }}"></script>
<script src="{{asset('assets/js/init/datatables-init.js') }}"></script>


<script type="text/javascript">
    $(document).ready(function() {
      $('#bootstrap-data-table-export').DataTable();
  } );
</script>

@endsection
@endsection
