<?php

use App\Http\Controllers\api\ConnexionController;
use App\Http\Controllers\api\TransactionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
  //  return $request->user();
//});

Route::post('/login', [ConnexionController::class,'loginUser']);
Route::post('/register', [ConnexionController::class,'registerUser']);
Route::post('/createTransaction', [TransactionController::class,'store']);
Route::post('/findEntrepriseNumber', [TransactionController::class,'findInformation']);
